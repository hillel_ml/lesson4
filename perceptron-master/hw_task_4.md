Используя приведенный на занятии код для классификации набора данных MNIST, добиться улучшения качества работы модели, основанной на персептроне, путем изменения:
1. dropout
2. функции активации
3. оптимизационного алгоритма

Составить отчет о настройке модели, привести комбинацию параметров, обеспечивающих наилучшие показатели качества.

Фреймворк можно использовать любой - TensorFlow или PyTorch.

Результат выполнения работы - код и отчет в архиве, названном согласно требованиям.

90 баллов - поиск оптимальной комбинации гиперпараметров осуществлен вручную, визуализация в TensorBoard.

100 баллов - поиск оптимальной комбинации гиперпараметров осуществлен c помощью Optuna, визуализация в TensorBoard.



https://pytorch.org/docs/stable/optim.html
Для Optuna (использует torch.optim из pytorch) можно использовать следующие оптимизаторы:
- Adadelta
- Adagrad
- Adam
- AdamW
- SparseAdam - неизвестная ошибка, не разбирался
- Adamax
- ASGD
- LBFGS - необходима функция closure
- NAdam
- RAdam
- RMSprop
- Rprop
- SGD


Some optimization algorithms such as Conjugate Gradient and LBFGS need to reevaluate the function multiple times, so you have to pass in a closure that allows them to recompute your model. The closure should clear the gradients, compute the loss, and return it.
Некоторые алгоритмы оптимизации, такие как Conjugate Gradient и LBFGS, должны повторно вычислять функцию несколько раз, поэтому вам нужно передать замыкание, которое позволит им пересчитать вашу модель. Замыкание должно очищать градиенты, вычислять потери и возвращать их.
Не проверенный пример
Example:

for input, target in dataset:
    def closure():
        optimizer.zero_grad()
        output = model(input)
        loss = loss_fn(output, target)
        loss.backward()
        return loss
    optimizer.step(closure)




https://www.tensorflow.org/api_docs/python/tf/keras/Model
https://www.tensorflow.org/api_docs/python/tf/keras/optimizers
https://www.tensorflow.org/api_docs/python/tf/keras/activations


При этом для TF можно использовать следующие оптимизаторы:
- Adadelta
- Adagrad
- Adam
- Adamax
- Ftrl - нет в оптуне
- Nadam
- Optimizer: Base class for Keras optimizers.
- RMSprop
- SGD




Built-in activation functions.

Functions
- deserialize(...): Returns activation function given a string identifier.
- elu(...): Exponential Linear Unit.
- exponential(...): Exponential activation function.
- gelu(...): Applies the Gaussian error linear unit (GELU) activation function.
- get(...): Returns function.
- hard_sigmoid(...): Hard sigmoid activation function.
- linear(...): Linear activation function (pass-through).
- relu(...): Applies the rectified linear unit activation function.
- selu(...): Scaled Exponential Linear Unit (SELU).
- serialize(...): Returns the string identifier of an activation function.
- sigmoid(...): Sigmoid activation function, sigmoid(x) = 1 / (1 + exp(-x)).
- softmax(...): Softmax converts a vector of values to a probability distribution.
- softplus(...): Softplus activation function, softplus(x) = log(exp(x) + 1).
- softsign(...): Softsign activation function, softsign(x) = x / (abs(x) + 1).
- swish(...): Swish activation function, swish(x) = x * sigmoid(x).
- tanh(...): Hyperbolic tangent activation function.






I suppose that you can solve the bug by replacing the line nonlinearity = trial.suggest_categorical("nonlinearity", [F.relu, F.celu, F.sigmoid, F.tanh, F.leaky_relu, F.gelu, F.hardswish]) as follows:

activation_candidates = {
    "relu": torch.nn.ReLU(),
    "celu": torch.nn.CELU(),
    "sigmoid": torch.nn.Sigmoid(),
    "tanh": torch.nn.Tanh(),
    "reakly_relu": torch.nn.LeakyReLU(),
    "gelu": torch.nn.GELU(),
    "hardswitch": torch.nn.Hardswish()
}

nonlinearity_name = trial.suggest_categorical("nonlinearity", list(activation_candidates))
nonlinearity = activation_candidates[nonlinearity_name]