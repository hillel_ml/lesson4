# Imports from optuna_nn_test
import optuna
import torch
import torch.nn as nn
import torch.optim as optim
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from pprint import pformat

# Imports from tf_board
import datetime
import tensorflow
from tensorflow.keras.datasets import mnist
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Dropout
from tensorflow.keras.utils import to_categorical




from keras.backend import clear_session
from keras.layers import Conv2D
from keras.layers import Flatten
from tensorflow.keras.optimizers import RMSprop



N_TRAIN_EXAMPLES = 3000
N_VALID_EXAMPLES = 1000
BATCHSIZE = 128
CLASSES = 10
EPOCHS = 10

# Configuration options
feature_vector_length = 784
num_classes = 10


# Load the data
(X_train, Y_train), (X_test, Y_test) = mnist.load_data()

df = pd.DataFrame(mnist.load_data())

# Reshape the data - MLPs do not understand such things as '2D'.
# Reshape to 28 x 28 pixels = 784 features
X_train = X_train.reshape(X_train.shape[0], feature_vector_length)
X_test = X_test.reshape(X_test.shape[0], feature_vector_length)

# Convert into greyscale
X_train = X_train.astype('float32')
X_test = X_test.astype('float32')
X_train /= 255
X_test /= 255

# Convert target classes to categorical ones
Y_train = to_categorical(Y_train, num_classes)
Y_test = to_categorical(Y_test, num_classes)

# Set the input shape
input_shape = (feature_vector_length,)

print(f'Feature shape: {input_shape}')

var_results = {
    'x_train': X_train,
    'y_train': Y_train,
    'x_test' : X_test,
    'y_test': Y_test
}

print(len(X_train))
print(len(Y_train))
print(len(X_test))
print(len(Y_test))


# df = pd.DataFrame.from_dict(var_results, orient='index')
# df = df.transpose()
# df = pd.get_dummies(df)


# train = X_train, Y_train
# val = X_test, Y_test


# # Build a model by implementing define-by-run design from Optuna
# def build_model_custom(trial):
    
#     n_layers = trial.suggest_int("n_layers", 1, 3)
#     layers = []

#     in_features = 20
    
#     for i in range(n_layers):
        
#         out_features = trial.suggest_int("n_units_l{}".format(i), 4, 18)
        
#         layers.append(nn.Linear(in_features, out_features))
#         layers.append(nn.LeakyReLU())

#         in_features = out_features
        
#     layers.append(nn.Linear(in_features, 2))
#     layers.append(nn.LeakyReLU())
    
#     return nn.Sequential(*layers)

# # Train and evaluate the accuracy of neural network with the addition of pruning mechanism
# def train_and_evaluate(param, model, trial):
    
#     # train_data, val_data = train_test_split(df, test_size = 0.2, random_state = 42)
#     # train, val = Dataset(train_data), Dataset(val_data)
#     train = X_train, Y_train
#     val = X_test, Y_test

#     train_dataloader = torch.utils.data.DataLoader(train, batch_size=2, shuffle=True)
#     val_dataloader = torch.utils.data.DataLoader(val, batch_size=2)

#     use_cuda = torch.cuda.is_available()
#     device = torch.device("cuda" if use_cuda else "cpu")

#     criterion = nn.CrossEntropyLoss()
#     optimizer = getattr(optim, param['optimizer'])(model.parameters(), lr= param['learning_rate'])

#     if use_cuda:

#             model = model.cuda()
#             criterion = criterion.cuda()

#     for epoch_num in range(EPOCHS):

#             total_acc_train = 0
#             total_loss_train = 0

#             for train_input, train_label in train_dataloader:

#                 train_label = train_label.to(device)
#                 train_input = train_input.to(device)

#                 output = model(train_input.float())
                
#                 batch_loss = criterion(output, train_label.long())
#                 total_loss_train += batch_loss.item()
                
#                 acc = (output.argmax(dim=1) == train_label).sum().item()
#                 total_acc_train += acc

#                 model.zero_grad()
#                 batch_loss.backward()
#                 optimizer.step()
            
#             total_acc_val = 0
#             total_loss_val = 0

#             with torch.no_grad():

#                 for val_input, val_label in val_dataloader:

#                     val_label = val_label.to(device)
#                     val_input = val_input.to(device)

#                     output = model(val_input.float())

#                     batch_loss = criterion(output, val_label.long())
#                     total_loss_val += batch_loss.item()
                    
#                     acc = (output.argmax(dim=1) == val_label).sum().item()
#                     total_acc_val += acc
            
#             accuracy = total_acc_val/len(val)
            
#             # Add prune mechanism
#             trial.report(accuracy, epoch_num)

#             if trial.should_prune():
#                 raise optuna.exceptions.TrialPruned()

#     return accuracy
  
# # Define a set of hyperparameter values, build the model, train the model, and evaluate the accuracy
# def objective(trial):

#     # Clear clutter from previous Keras session graphs.
#     clear_session()

#     params = {
#             'learning_rate': trial.suggest_loguniform('learning_rate', 1e-5, 1e-1),
#             'dropout': trial.suggest_float("dropout", 0.1, 1.0),
#             'optimizer': trial.suggest_categorical("optimizer", ["Adadelta",
#                                                                 "Adagrad",
#                                                                 "Adam",
#                                                                 "Adamax",
#                                                                 "NAdam",
#                                                                 "RMSprop",
#                                                                 "SGD"
#                                                                 ]),
#             'activation': trial.suggest_categorical("activation", ["relu", "linear"]),
#             'n_unit': trial.suggest_int("n_unit", 4, 18)
#             }
    
#     model = build_model_custom(trial)

#     accuracy = train_and_evaluate(params, model, trial)

#     return accuracy
  
  
# EPOCHS = 30
# fin_results = {}
    
# study = optuna.create_study(direction="maximize", sampler=optuna.samplers.TPESampler(), pruner=optuna.pruners.MedianPruner())
# study.optimize(objective, n_trials=30)


# best_trial = study.best_trial

# for key, value in best_trial.params.items():
#     fin_results[key] = value

# print(fin_results)
# print(fin_results['dropout'])















# # Start tf_board





# # Create the model
# model = Sequential()
# model.add(Dense(350, input_shape=input_shape, activation='relu'))
# model.add(Dense(50, activation='relu'))
# model.add(Dense(num_classes, activation='softmax'))
# model.add(Dropout(fin_results['dropout']))

# # Configure the model and start training
# model.compile(loss='categorical_crossentropy', optimizer=fin_results['optimizer'], metrics=['accuracy'])

# log_dir = "logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
# tensorboard_callback = tensorflow.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)


# model.fit(X_train, Y_train, epochs=10, batch_size=250, verbose=1, validation_split=0.2, callbacks=[tensorboard_callback])

# # Test the model after training
# test_results = model.evaluate(X_test, Y_test, verbose=1)
# print(f'Test results - Loss: {test_results[0]} - Accuracy: {test_results[1]}%')